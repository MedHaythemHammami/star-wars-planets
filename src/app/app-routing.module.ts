import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListPlanetsComponent } from './list-planets/list-planets.component';
import { PlanetDetailsComponent } from './planet-details/planet-details.component';


const routes: Routes = [
  { path: '',redirectTo: 'list-planet', pathMatch: 'full'},
  { path: 'list-planet', component: ListPlanetsComponent },
  { path: 'planet-detail/:id', component: PlanetDetailsComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
