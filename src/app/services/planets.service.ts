import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PlanetsService {

  constructor(public http: HttpClient) { 
  }

  getAllPlanets(){
   return this.http.get('https://swapi.co/api/planets')
  }
  
  getPlanetById(id){
    return this.http.get('https://swapi.co/api/planets/'+id)
  }

}
