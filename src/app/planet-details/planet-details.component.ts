import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PlanetsService } from '../services/planets.service';

@Component({
  selector: 'app-planet-details',
  templateUrl: './planet-details.component.html',
  styleUrls: ['./planet-details.component.css']
})
export class PlanetDetailsComponent implements OnInit {
  planetInformation : any = null;

  constructor(
    public route: ActivatedRoute,
    public planetsService:PlanetsService
  ) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.getPlanetById(params['id']) ;
    });
  }

  getPlanetById(id){
    this.planetsService.getPlanetById(id).subscribe(data=>{
      this.planetInformation = data
    })
  }

}
