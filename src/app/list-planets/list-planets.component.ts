import { Component, OnInit } from '@angular/core';
import { PlanetsService } from '../services/planets.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-list-planets',
  templateUrl: './list-planets.component.html',
  styleUrls: ['./list-planets.component.css']
})
export class ListPlanetsComponent implements OnInit {
  //list of planets
  planets : any ;

  constructor(
    public planetsService:PlanetsService,
    public router : Router
  ) { }

  ngOnInit() {
    this.getAllPlanets();
  }
  getAllPlanets(){
    this.planetsService.getAllPlanets().subscribe((data:any)=>{
      this.planets = data.results
      console.log(this.planets)
    })
  }
  goToDetail(url){
    var id = url.substring(0, url.length - 1).split("/").pop()
    this.router.navigate(['/planet-detail/',id]);
  }

}
